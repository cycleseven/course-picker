import babelRegister from 'babel-core/register.js';
import babelResolver from 'babel-resolver';
import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import jsdom from 'jsdom';

const doc = jsdom.jsdom('<!doctype html><html><body></body></html>');
const win = doc.defaultView;

global.document = doc;
global.window = win;

Object.keys(window).forEach((key) => {
  if (!(key in global)) {
    global[key] = window[key];
  }
});

chai.use(chaiImmutable);

babelRegister({presets: ['es2015', 'react'], resolveModuleSource: babelResolver('src')});
