import React from 'react';
import ReactDOM from 'react-dom';
import {createStore} from 'redux';

import App from 'components/App.jsx';
import {coursePickerApp} from 'reducers.js';

createStore(coursePickerApp);

ReactDOM.render(
  <App />,
  document.getElementById('app')
);
