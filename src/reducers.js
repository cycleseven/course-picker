import {fromJS} from 'immutable';

import {ADD_COURSE} from 'actions.js';

const initialState = {courses: []};

export function coursePickerApp(state = initialState, action) {
  switch (action.type) {
    case ADD_COURSE: {
      const newCourse = fromJS(action.payload.course);
      const updatedCourses = state.get('courses').push(newCourse);
      return state.set('courses', updatedCourses);
    }
    default:
      return state;
  }
}
