import {expect} from 'chai';
import {fromJS} from 'immutable';

import {ADD_COURSE} from 'actions.js';
import {coursePickerApp} from 'reducers.js';

describe('ADD_COURSE reducer', () => {
  it('should append the course to the list of courses', () => {
    const initialState = fromJS({
      courses: [
        {
          credits: 10,
          title: 'Functional Programming'
        }
      ]
    });

    const action = {
      type: ADD_COURSE,
      payload: {
        course: {
          credits: 40,
          title: 'Fundamentals of Macroeconomics'
        }
      }
    };

    const nextState = coursePickerApp(initialState, action);
    expect(nextState).to.equal(fromJS({
      courses: [
        {
          credits: 10,
          title: 'Functional Programming'
        },
        {
          credits: 40,
          title: 'Fundamentals of Macroeconomics'
        }
      ]
    }));
  });
});
