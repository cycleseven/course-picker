// Action types

export const ADD_COURSE = 'ADD_COURSE';


// Action creators

export function addCourse(course) {
  return {type: ADD_COURSE, payload: {course: course}};
}
