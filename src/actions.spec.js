import {expect} from 'chai';
import {fromJS} from 'immutable';

import {ADD_COURSE, addCourse} from 'actions.js';

describe('addCourse action creator', () => {
  it('should return an ADD_COURSE action', () => {
    const course = {
      credits: 20,
      title: 'Introduction to Linear Algebra'
    };

    const action = addCourse(course);

    expect(fromJS(action)).to.equal(fromJS({
      type: ADD_COURSE,
      payload: {
        course: {
          credits: 20,
          title: 'Introduction to Linear Algebra'
        }
      }
    }));
  });
});
