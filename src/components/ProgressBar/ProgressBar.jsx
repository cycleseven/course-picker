import React from 'react';
import styleable from 'react-styleable';

import css from 'components/ProgressBar/ProgressBar.css';

class ProgressBar extends React.Component {
  render() {
    return <div className={this.props.css.background}>
      <div
        className={this.props.css.bar}
        style={{width: 100 * (this.props.complete / this.props.total) + '%'}}>
      </div>
    </div>;
  }
}

ProgressBar.propTypes = {
  complete: React.PropTypes.number,
  total: React.PropTypes.number
};

ProgressBar.defaultProps = {
  complete: 0,
  total: 100
};

export default styleable(css)(ProgressBar);
