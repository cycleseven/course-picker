import React from 'react';
import {renderIntoDocument} from 'react-addons-test-utils';

import ProgressBar from 'components/ProgressBar/ProgressBar.jsx';

describe('ProgressBar component', () => {
  it('should render', () => {
    renderIntoDocument(<ProgressBar />);
  });
});
