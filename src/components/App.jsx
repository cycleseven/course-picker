import React from 'react';
import 'normalize.css';

import theme from 'Theme.css';
import ProgressBar from 'components/ProgressBar/ProgressBar.jsx';

class App extends React.Component {
  render() {
    return <ProgressBar css={theme} complete={20} total={100} />;
  }
}

export default App;
